<?php get_header(); ?>

<div id="leftCol">
    <?php 
        if ( have_posts() ) {
            while ( have_posts() ) {
                
                echo "<div id=\"post\">";
                    the_post();
                    
                    echo "<h3>";
                    the_title(); 
                    echo "</h3>";
                    
                    echo "<i>";
                    the_field("responsavel_pelo_teste");
                    echo "<i>";

                    the_content(); 
                echo "</div>";
            }
        }else{
            echo "Nos desculpe, não há Posts publicados!";
        }
    
    ?>
</div>
<div id="rightCol">
<?php 
    query_posts(array(
    'post_type' => 'news'
    ));
    while (have_posts()) : the_post(); 
?>
        <h3><?php the_title(); ?></h3>
        <p><?php the_excerpt(); ?></p>
    <?php endwhile; ?>
</div>
<?php get_footer(); ?>