<?php

function create_posttype() {

    register_post_type( 'news',
        array(
            'labels' => array(
            'name' => __( 'Test Post Type' ),
            'singular_name' => __( 'News' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'news'),
            )
        );

}

add_action( 'init', 'create_posttype' );


